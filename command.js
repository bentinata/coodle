'use strict';

var command = {};

command.average = function(start, end){
  start = Number(start);
  end = Number(end);

  var count = 0,
  sum = 0;

  var r = {
    start: start,
    end: end,
  }

  while(start<=end){
    count++;
    sum+=start;
    start++;
  }

  r.count = count;
  r.sum = sum;
  r.average = sum/count;

  return r;
}

command.wordfromnum = function(n){
var string = n.toString(), units, tens, scales, start, end, chunks, chunksLen, chunk, ints, i, word, words, and = 'and';

  /* Is number zero? */
  if( parseInt( string ) === 0 ) {
    return 'zero';
  }

  /* Array of units as words */
  var units = [ '', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen' ];

  /* Array of tens as words */
  var tens = [ '', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety' ];

  /* Array of scales as words */
  var scales = [ '', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion', 'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quatttuor-decillion', 'quindecillion', 'sexdecillion', 'septen-decillion', 'octodecillion', 'novemdecillion', 'vigintillion', 'centillion' ];

  /* Split user arguemnt into 3 digit chunks from right to left */
  var start = string.length;
  var chunks = [];
  while( start > 0 ) {
    var end = start;
    chunks.push( string.slice( ( start = Math.max( 0, start - 3 ) ), end ) );
  }

  /* Check if function has enough scale words to be able to stringify the user argument */
  var chunksLen = chunks.length;
  if( chunksLen > scales.length ) {
    return '';
  }

  /* Stringify each integer in each chunk */
  var words = [], word;
  for( i = 0; i < chunksLen; i++ ) {

    chunk = parseInt( chunks[i] );

    if( chunk ) {

      /* Split chunk into array of individual integers */
      var ints = chunks[i].split( '' ).reverse().map( parseFloat );

      /* If tens integer is 1, i.e. 10, then add 10 to units integer */
      if( ints[1] === 1 ) {
        ints[0] += 10;
      }

      /* Add scale word if chunk is not zero and array item exists */
      if( ( word = scales[i] ) ) {
        words.push( word );
      }

      /* Add unit word if array item exists */
      if( ( word = units[ ints[0] ] ) ) {
        words.push( word );
      }

      /* Add tens word if array item exists */
      if( ( word = tens[ ints[1] ] ) ) {
        words.push( word );
      }

      /* Add 'and' string after units or tens integer if: */
      if( ints[0] || ints[1] ) {

        /* Chunk has a hundreds integer or chunk is the first of multiple chunks */
        if( ints[2] || ! i && chunksLen > 1 ) {
          words.push( and );
        }

      }

      /* Add hundreds word if array item exists */
      if( ( word = units[ ints[2] ] ) ) {
        words.push( word + ' hundred' );
      }

    }

  }

  return words.reverse().join(' ');
}

command.wordvalue = function(word) {
  word = word.replace(/[^a-z]/g,'');
  var sum = 0;
  for(var i=0, j=word.length; i<j; i++){
    sum+=word.charCodeAt(i)-96;
  }
  return sum;
}

command.loop = function(looptimes) {
  var samevalue = [];
  var word, value;
  looptimes = looptimes || 0;


  for (var i = 0; i < looptimes; i++){
    word = command.wordfromnum(i)
    value = command.wordvalue(word)
    // console.log(i, value)
    if(i===command.wordvalue(word)){
      samevalue.push({number: i, word: word});
    }
  }

  return samevalue;
}

command.obj = function() {
  var person = {
    firstname: 'Ben',
    lastname: 'Nata',
    fullname: function(){
      return this.firstname + ' ' + this.lastname
    }
  };

  return person.fullname
}

var command = {};

command.minstr = function(input, movement) {
  var allowed = [];
  // for (var i=32; i<127; i++) {
  for (var i=65; i<92; i++) {
    var character = String.fromCharCode(i);
    if (!/[.$#\[\]\/]/.test(character)) {
      allowed.push(String.fromCharCode(i));
    }
  }

  // return allowed;
  return moveStr(input, movement, allowed);
}

function moveStr(word, n, list) {
  var modified = [];
  n = parseInt(n);
  moveChar(word.length-1, n)

  function moveChar(charIdx, move) {
    if (charIdx < 0 && !move) return;

    var idx = (list.indexOf(word[charIdx]) + move) % list.length;
    var extra = Math.floor((list.indexOf(word[charIdx]) + move) / list.length);
    var newc = list[idx % list.length];
    // console.log(charIdx)
    console.log('changing ', word[charIdx], ' to ', newc, ' movement ', move, ' extra ', extra)
    modified.push(newc);
    moveChar(charIdx-1, extra);
  }

  return modified.reverse().join('');
}

console.log(command[process.argv[2]].apply(null, process.argv.slice(3)));
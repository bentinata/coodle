module.exports = {
  anonFn: function() {
    return true;
  },

  namedFn: function named() {
    return true;
  },

  shortFn() {
    return true;
  },

  arrowFn: () => true,
}
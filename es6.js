'use strict';

function normal(arg) {
  try {
    return [];
  } catch (e) {
    return e;
  }

}

let arrow = (arg) => arg;

const object = {
  1: 'numeric',
  '2': 'string',
  [arrow(10)]: 'dynamic',
  otherArrow: (arg) => arg,
  func(arg) {
    return arg;
  },
}

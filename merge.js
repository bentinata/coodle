const merge = require(`lodash/merge`);
const fpMerge = require(`lodash/fp/merge`);

const parent = { name: `parent`, get val() { return this.name }};
const child = { name: `child` };

console.log(`expected: ${merge(parent, child).val}`);
console.log(`reversed: ${merge(child, parent).val}`);
console.log(`fp: ${merge(parent, child).val}`);
console.log(`fp reversed arity: ${merge(child, parent).val}`);

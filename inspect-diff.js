'use strict'

const diff = require(`difflet`)({ indent: 2, comment: true })
const normal = require(`./normal.json`)
const inspect = require(`./inspect.json`)

process.stdout.write(diff.compare(normal, inspect))


#!/usr/bin/env node
'use strict';

function those() {
  console.log(arguments.callee.name, 'called by', arguments.callee.caller.name);
  if (arguments.callee.caller.name === 'test')
    that();
}

function that() {
  console.log(arguments.callee.name, 'called by', arguments.callee.caller.name);
  if (arguments.callee.caller.name === 'test')
    those();
}

var variable = 'something';
const constant = 'other';
// let imunsure = 'any';

// const thing = this;

console.log(arguments);
